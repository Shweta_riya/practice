
<?PHP
require_once("../../../vendor/autoload.php");
use App\City\City;
use  App\Message\Message;
use \App\Utility\Utility;

$objCity=new City();

$allData=$objCity->index("obj");
################## search  block 1 of 5 start ##################

if(isset($_REQUEST['search']) )$allData =  $objCity->search($_REQUEST);
$availableKeywords=$objCity->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';

################## search  block 1 of 5 end ##################
######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objCity->indexPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################
################## search  block 2 of 5 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $objCity->search($_REQUEST);
    $serial = 1;
}
################## search  block 2 of 5 end ##################


?>

<!-- required for search, block3 of 5 start -->

<link rel="stylesheet" href="../../../resource/bootstrap/css/jquery-ui.css">
<script src="../../../resource/assets/bootstrap/js/jquery-1.12.4.js"></script>
<script src="../../../resource/assets/bootstrap/js/jquery-ui.js"></script>

<!-- required for search, block3 of 5 end -->
<html>

<body background="image1.jpg" >


<table  align="center" style="background-color: white"  >
    <tr>
        <td width="2500">
            <h2>Active List of City</h2>
        </td>
        <td width="2500">
            <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
            <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
            <a href="email.php?list=1" class="btn btn-primary" role="button">Email to friend</a>

        </td>
        <td width="150">


            <!-- required for search, block 4 of 5 start -->

            <div class="input-group col-md-12" style="float: right">

                <form id="searchForm" action="index.php"  method="get">
                    <input type="text" value="" id="searchID" class="search-query " name="search" placeholder="Search" >
                        <span class="input-group-btn">
                            <button class="btn btn-danger" type="button" value="search">
                                <span class="fa fa-search"></span>
                            </button>
                        </span><br>
                    <input type="checkbox"  name="byName"   checked  >By Name
                    <input type="checkbox"  name="byCity"  checked >By City
                </form>

            </div>
            <!-- required for search, block 4 of 5 end -->


        </td>
    </tr>
    </br>
</table>
</br>



<div id="TopMenuBar">
    <button type="button" onclick="window.location.href='../index.php'" class=" btn-primary btn-lg">Home</button>
    <button type="button" onclick="window.location.href='create.php'" class=" btn-primary btn-lg">Add new</button>
    <button type="button" onclick="window.location.href='trashed.php?Page=1'" class=" btn-success btn-lg">Trashed List</button>
</div>
</br>
</body>

</html>

<?php

echo "<table align='center' style='background-color: #2aabd2'>";

echo"<center><th> serial</th><th> ID</th><th>name</th><th>City name</th><th>Action</th><center>";
foreach($someData as $OneData){
    echo"<tr>";
    echo "<td>$serial</td> ";
    echo "<td>$OneData->id</td> ";
    echo "<td>$OneData->name</td> ";
    echo "<td>$OneData->city_name</td> ";
    echo"
    <td>
    <a href='view.php?id=$OneData->id'><button class='btn btn-info'>view</button></a>
    <a href='edit.php?id=$OneData->id'><button class='btn btn-success'>edit</button></a>
    <a href='delete.php?id=$OneData->id'><button class='btn btn-danger'>delete</button></a>
    <a href='trash.php?id=$OneData->id'><button class='btn btn-sucess'>trash</button></a>


    </td>
    ";
    $serial++;


    echo"</tr>";

}
echo"</table>";
?>

<html>
<!--  ######################## pagination code block#2 of 2 start ###################################### -->
<div align="center" class="container" style="background-color: white">
    <ul class="pagination">

        <?php

        $pageMinusOne  = $page-1;
        $pagePlusOne  = $page+1;
        if($page>$pages) Utility::redirect("index.php?Page=$pages");

        if($page>1)  echo "<li><a href='index.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";
        for($i=1;$i<=$pages;$i++)
        {
            if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
            else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

        }
        if($page<$pages) echo "<li><a href='index.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

        ?>

        <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value; " >
            <?php
            if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

            if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
            else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

            if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

            if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

            if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

            if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
            else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
            ?>
        </select>
    </ul>
</div>
<!--  ######################## pagination code block#2 of 2 end ###################################### -->

</html>



<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->