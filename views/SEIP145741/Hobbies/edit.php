<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset ($_SESSION))session_start();
echo Message::getMessage();



use App\Hobbies\Hobbies;
$objHobbies=new Hobbies();
$objHobbies->setData($_GET);
$oneData=$objHobbies->view("obj");
$arrHobbies = explode(',',$oneData->hobby);

?>





<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap Login Form Template</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../resource/assets/css/style.css">





    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../../../resource/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="../../../resource/https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="../../../resource/assets/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../../resource/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../../resource/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../../resource/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../../../resource/assets/ico/apple-touch-icon-57-precomposed.png">

</head>

<body>

<!-- Top content -->
<div class="top-content">

    <div class="container">

        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Checklist -Edit</h3>
                        <p>Edit name and checklist:</p>
                    </div>
                    <div class="form-top-right">

                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="update.php" method="post" class="login-form">
                        <input type="hidden" name="id" value="<?PHP echo $oneData->id?>">
                        <div class="form-group">
                            <label class="sr-only" for="form-username">Name</label>
                            <input type="text" name="name" value="<?PHP echo $oneData->name?>" class="form-username form-control" id="form-username">
                        </div>
                        <div class="container">

                            <label class="checkbox-inline">
                                <input type="checkbox" name="hobby[]" value="football" <?php if(in_array("football",$arrHobbies)){echo "checked";} ?>>football
                            </label>

                            <label class="checkbox-inline">
                                <input type="checkbox" name="hobby[]" value="cricket" <?php if(in_array("cricket",$arrHobbies)){echo "checked";} ?>>cricket
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="hobby[]"value="writing" <?php if(in_array("writing",$arrHobbies)){echo "checked";} ?>>writing
                            </label>
                        </div>
                        <button type="submit" class="btn">Update !</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>




<!-- Javascript -->
<script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../resource/assets/js/jquery.backstretch.min.js"></script>
<script src="../../../resource/assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../../resource/assets/js/placeholder.js"></script>
<![endif]-->

</body>

</html>