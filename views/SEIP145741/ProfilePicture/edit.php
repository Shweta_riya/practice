<?php
use App\ProfilePicture\ProfilePicture;
require_once("../../../vendor/autoload.php");

$pic = new ProfilePicture;
$pic->setData($_GET);
$oneData =  $pic->view('obj');

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Atomic Project</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lobster|Merriweather|Montserrat|Shrikhand" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="../https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<div class="container-fluid wrapper">


    <hr class="hr-divider">
    <hr class="hr-divider">
    <div class="container-fluid wrapper">
        <div class="row">

            <div class="col-md-10 ">
                <div class="atomic-nav">
                    <div class="navbar">
                        <div class="container">
                            <div class="navbar-header">

                            </div>
                            <div class="btn-group-lg nav navbar-nav" role="group" aria-label="...">
                                <a href="create.php" class="navbar-btn btn btn-info">Add Item</a>
                                <a href="" class="navbar-btn btn btn-warning">Trash Item</a>

                            </div>
                        </div>
                    </div>

                </div>


                <form class="form-horizontal my-form " action="update.php" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <legend class="project-sub-heading">Edi Profile Picture</legend>

                        <div class="form-group">
                            <label for="user" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="hidden" name="id" value="<?php echo $oneData->id ?>" >
                                <input type="text" class="form-control" id="name" name="user" value="<?php echo $oneData->name ?>" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="date" class="col-sm-2 control-label">profilepic</label>
                            <div class="col-sm-10">
                                <img src="<?php echo $oneData->profile_image ?>" alt="" width="100" height="100">
                                <input type="file" class="form-control" name="profilepic" placeholder="Profile Picture">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="submit" class="btn btn-success" value="Save">
                              
                            </div>
                        </div>
                    </fieldset>
                </form>

            </div>
        </div>
    </div>
    <hr class="hr-divider">
    <div class="footer">
        <div class="row">

            <div class="user-img col-md-6">
            </div>
        </div>
    </div>
</div>




<!-- Javascript -->
<script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>



</body>

</html>
