<?php

require_once("../../../vendor/autoload.php");
use App\ProfilePicture\ProfilePicture;
use  App\Message\Message;
use App\Utility\Utility;


$objProfilePicture = new ProfilePicture;
$allData =  $objProfilePicture->index("obj");
################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$allData =  $objProfilePicture->search($_REQUEST);
$availableKeywords=$objProfilePicture->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################
######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objProfilePicture->indexPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################
################## search  block 2 of 5 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $objProfilePicture->search($_REQUEST);
    $serial = 1;
}
################## search  block 2 of 5 end ##################

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Atomic Project</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lobster|Merriweather|Montserrat|Shrikhand" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">



</head>
<body >


<table  align="center" style="background-color: gray"  >
    <tr>
        <td width="2500">
            <h2>Active List of profile</h2>
        </td>
        <td width="2500">
            <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
            <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
            <a href="email.php?list=1" class="btn btn-primary" role="button">Email to friend</a>

        </td>
        <td width="150">


            <!-- required for search, block 4 of 5 start -->

            <div class="input-group col-md-12" style="float: right">

                <form id="searchForm" action="index.php"  method="get">
                    <input type="text" value="" id="searchID" class="search-query " name="search" placeholder="Search" >
                        <span class="input-group-btn">
                            <button class="btn btn-danger" type="button" value="search">
                                <span class="fa fa-search"></span>
                            </button>
                        </span><br>
                    <input type="checkbox"  name="byProfile"   checked  >By Profile
                    <input type="checkbox"  name="byName"  checked >By Name
                </form>

            </div>
            <!-- required for search, block 4 of 5 end -->


        </td>
    </tr>
    </br>
</table>
</br>



<div id="TopMenuBar">
    <button type="button" onclick="window.location.href='../index.php'" class=" btn-primary btn-lg">Home</button>
    <button type="button" onclick="window.location.href='create.php'" class=" btn-primary btn-lg">Add new</button>
    <button type="button" onclick="window.location.href='trashed.php?Page=1'" class=" btn-success btn-lg">Trashed List</button>
</div>
</br>
</body>
</html>




<html>
<body>
<div class="container-fluid wrapper">


</div>



<div class="col-md-10 ">
    <div class="atomic-nav">
        <div class="navbar">
            <div class="container">
                <div class="navbar-header">

                </div>
                <div class="btn-group-lg nav navbar-nav" role="group" aria-label="...">
                    <a href="create.php" class="navbar-btn btn btn-info">Add Item</a>
                    <a href="trash_item.php" class="navbar-btn btn btn-warning">Trash Item</a>

                </div>
            </div>
        </div>

    </div>
    <div class="col-md-12 ">

        <?php

        echo "<table align='center' border='1' style='background-color: #2b669a' >";

        echo"<center><th> serial</th><th> ID</th><th>Name</th><th>profile image</th><th>Action</th><center>";
        foreach($someData as $OneData){
            echo"<tr>";
            echo "<td>$serial</td> ";
            echo "<td>$OneData->id</td> ";
            echo "<td>$OneData->name</td> ";
            echo "<td>$OneData->profile_image</td> ";
            echo"
    <td>
    <a href='view.php?id=$OneData->id'><button class='btn btn-info'>view</button></a>
    <a href='edit.php?id=$OneData->id'><button class='btn btn-success'>edit</button></a>
    <a href='delete.php?id=$OneData->id'><button class='btn btn-danger'>delete</button></a>
    <a href='trash.php?id=$OneData->id'><button class='btn btn-sucess'>trash</button></a>


    </td>
    ";
            $serial++;


            echo"</tr>";

        }
        echo"</table>";
        ?>
    </div>



</div>

<hr class="hr-divider">
<div class="footer">
    <div class="row">

        <div class="user-img col-md-6">
        </div>
    </div>
</div>






<!-- Javascript -->
<script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>

<script>
    $("#alertmsg").fadeTo(2000, 500).slideUp(500, function(){
        $("#alertmsg").slideUp(500);
    });

</script>


</body>


</html>

<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->
