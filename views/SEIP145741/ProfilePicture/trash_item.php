<?php

require_once("../../../vendor/autoload.php");
use App\ProfilePicture\ProfilePicture;
use App\Utility\Utility;
$pic = new ProfilePicture;


$allData =  $pic->trashitem('obj');

################# search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$allData =  $pic->search($_REQUEST);
$availableKeywords=$pic->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################

######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $pic->indexPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################

################## search  block 2 of 5 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $pic->search($_REQUEST);
    $serial = 1;
}
################## search  block 2 of 5 end ##################
?>
<!Doctype html>
<script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="../../../resource/assets/css/form-elements.css">
<link rel="stylesheet" href="../../../resource/assets/css/style.css">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
<link rel="stylesheet" href="../../../resource/assets/bootstrap/css/s.css">

<!-- required for search, block3 of 5 start -->

<link rel="stylesheet" href="../../../resource/assets/bootstrap/css/jquery-ui.css">
<script src="../../../resource/assets/bootstrap/js/jquery-1.12.4.js"></script>
<script src="../../../resource/assets/bootstrap/js/jquery-ui.js"></script>

<!-- required for search, block3 of 5 end -->

<body>





<div align="right">
    <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
    <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
    <a href="email.php?list=1" class="btn btn-primary" role="button">Email to friend</a>



    <!-- required for search, block 4 of 5 start -->

    <br><br>

    <form id="searchForm" action="trash_item.php"  method="get">

        <input type="text" value="" id="searchID" name="search" placeholder="Search" width="60" ><br>
        <input type="checkbox"  name="byName"   checked  >By Name
        <input type="checkbox"  name="byImage"  checked >By Image
        <input hidden type="submit" class="btn-primary" value="search">

    </form>
</div>

<!-- required for search, block 4 of 5 end -->






<div align="center">
    <div id="TopMenuBar">
        <button type="button" onclick="window.location.href='../index.php'" class=" btn-info btn-lg">Home</button>
        <button type="button" onclick="window.location.href='create.php'" class=" btn-info btn-lg">Add new</button>
        <button type="button" onclick="window.location.href='trashed.php?Page=1'" class=" btn-info btn-lg">Trashed List</button>
    </div>
</div>
<br>
        <?php

        $serial =1;

        echo "<table class=\"table table-bordered table-hover my-table-border my-td\">";

        echo "<tr><th>Serial</th><th>ID</th><th>Name</th><th>Picture</th><th>Action</th></tr>";

        foreach($someData as $oneData){

            echo "<tr>";
            echo "<td>$serial</td>";
            echo "<td>$oneData->id</td>";
            echo "<td>$oneData->name</td>";
            echo "<td><img src='$oneData->profile_image' alt='' width='200'></td>";
            echo "<td><a href='view.php?id=$oneData->id'><button class=\"btn btn-info btn-sm\">View</button></a>
<a href='edit.php?id=$oneData->id'><button class=\"btn btn-primary btn-sm\">Edit</button></a>

<a href='trash.php?id=$oneData->id'> <button class=\"btn btn-warning btn-sm\">Trash</button>
                </a></td>";
            echo "</tr>";
            $serial++;
        }



        ?>


<!--  ######################## pagination code block#2 of 2 start ###################################### -->
<div align="center" class="container">
    <ul class="pagination">

        <?php

        $pageMinusOne  = $page-1;
        $pagePlusOne  = $page+1;
        if($page>$pages) Utility::redirect("trash_item.php?Page=$pages");

        if($page>1)  echo "<li><a href='trash_item.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";
        for($i=1;$i<=$pages;$i++)
        {
            if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
            else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

        }
        if($page<$pages) echo "<li><a href='trash_item.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

        ?>

        <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value; " >
            <?php
            if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

            if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
            else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

            if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

            if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

            if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

            if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
            else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
            ?>
        </select>
    </ul>
</div>
<!--  ######################## pagination code block#2 of 2 end ###################################### -->

</body>


</html>
<!--  ######################## pagination code block#2 of 2 end ###################################### -->


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="../../../resource/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="../../../resource/https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Favicon and touch icons -->
<link rel="shortcut icon" href="../../../resource/assets/ico/favicon.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../../resource/assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../../resource/assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../../resource/assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="../../../resource/assets/ico/apple-touch-icon-57-precomposed.png">

<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
