<?PHP
require_once("../../../vendor/autoload.php");
use App\BookTitle\BookTitle;
use  App\Message\Message;

$objBookTitle=new BookTitle();

$allData=$objBookTitle->index("obj");

######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objBookTitle->indexPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################
?>
<script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="../../../resource/assets/css/form-elements.css">
<link rel="stylesheet" href="../../../resource/assets/css/style.css">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
<link rel="stylesheet" href="../../../resource/assets/bootstrap/css/s.css">

<?php
echo "<table align='center'>";

echo"<center><th> serial</th><th> ID</th><th>Book title</th><th>Author name</th><th>Action</th><center>";
foreach($someData as $OneData){
  echo"<tr>";
  echo "<td>$serial</td> ";
    echo "<td>$OneData->id</td> ";
    echo "<td>$OneData->book_title</td> ";
    echo "<td>$OneData->author_name</td> ";
    echo"
    <td>
    <a href='view.php?id=$OneData->id'><button class='btn btn-info'>view</button></a>
    <a href='edit.php?id=$OneData->id'><button class='btn btn-success'>edit</button></a>
    <a href='delete.php?id=$OneData->id'><button class='btn btn-danger'>delete</button></a>
    <a href='trash.php?id=$OneData->id'><button class='btn btn-sucess'>trash</button></a>


    </td>
    ";
  $serial++;


  echo"</tr>";

}
echo"</table>";
?>

<html>

<!--  ######################## pagination code block#2 of 2 start ###################################### -->
<div align="center" class="container">
  <ul class="pagination">

    <?php
    echo '<li><a href="">' . "Previous" . '</a></li>';
    for($i=1;$i<=$pages;$i++)
    {
      if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
      else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

    }
    echo '<li><a href="">' . "Next" . '</a></li>';
    ?>

    <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
      <?php
      if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
      else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

      if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
      else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

      if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
      else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

      if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
      else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

      if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
      else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

      if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
      else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
      ?>
    </select>
  </ul>
</div>
<!--  ######################## pagination code block#2 of 2 end ###################################### -->

</html>