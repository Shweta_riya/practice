<?php

namespace App\SummaryofOrganization;
use App\Message\Message;
use App\Model\database as db;
use App\Utility\Utility;
use PDO;

//require_once("../../../../vendor/autoload.php");




class SummaryofOrganization extends db{
    public $id;
    public $name;
    public $comment;
    public function __construct()
    {
        parent::__construct();
    }
    public function setData($data = Null)
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];

        }
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];

        }
        if (array_key_exists('comment', $data)) {
            $this->comment= $data['comment'];

        }

    }
    public function store(){
        $arrData=array($this->name,$this->comment);

        $sql= "Insert INTO summary(name,comment) VALUES (?,?)";

        $STH= $this->DBH->prepare($sql);

        $result= $STH->execute($arrData);

        if($result)
            Message::setMessage("Sucess!data has been inserted sucessfully");
        else
            Message::setMessage("Failure!data has not been inserted sucessfully");
        Utility::redirect('create.php');
    }// end of store method
    public function index($fetchMode='ASSOC'){
        $sql = "SELECT * from summary where is_deleted = 0 ";

        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();
    public function view($fetchMode='ASSOC'){
        $sql='SELECT * from summary WHERE id='.$this->id;
        $STH = $this->DBH->query($sql);
        //echo $sql;


        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();


    public function update(){
        $arrData=array($this->name,$this->comment);

        //UPDATE `atomic_project_35`.`book_title` SET `book_title` = 'nan' WHERE `book_title`.`id` = 4;

        $sql="update summary  SET  name=? ,comment=? WHERE id=".$this->id;
        $STH =$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');
    }//end of update();

    public function delete(){
        $sql="Delete from summary where id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('index.php');

    }//end of delete();

    public function trash(){

        $sql = "Update summary SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()

    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from summary where is_deleted <> '0' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();
    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from summary WHERE is_deleted = '0' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }// end of index paginator
    public function trashedPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from summary WHERE is_deleted <> '0' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of trashedPaginator();

    public function recover(){

        $sql = "Update summary SET is_deleted='0' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('trashed.php');

    }// end of recover();


    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byComment']) )  $sql = "SELECT * FROM `summary` WHERE `is_deleted` ='0' AND (`name` LIKE '%".$requestArray['search']."%' OR `comment` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byComment']) ) $sql = "SELECT * FROM `summary` WHERE `is_deleted` ='0' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byComment']) )  $sql = "SELECT * FROM `summary` WHERE `is_deleted` ='0' AND `comment` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }// end of search()



    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `summary` WHERE `is_deleted` ='0'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->comment);
        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->comment);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords



}
//$objSummaryoforganization=new Summaryoforganization();

?>